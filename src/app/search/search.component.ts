import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from "@angular/core";
import { RadSideDrawer } from "nativescript-ui-sidedrawer";
import * as app from "tns-core-modules/application";
import { Color } from "tns-core-modules/color";
import { View } from "tns-core-modules/ui/page";
import * as SocialShare from "nativescript-social-share";
import { NoticiasService } from "../domain/noticias.service";
import * as Toast from "nativescript-toasts";
import { Noticia, NuevaNoticiaAction } from "../domain/noticias-state.model";
import { Store } from "@ngrx/store";
import { AppState } from "../app.module";




@Component({
    selector: "Search",
    moduleId: module.id,
    templateUrl: "./search.component.html"/*,
    providers: [NoticiasService]*/
})
export class SearchComponent implements OnInit {
    @ViewChild("layout", {static: false}) layout: ElementRef;
    resultados: Array<string>;
    constructor(private noticias: NoticiasService,
                private store: Store<AppState>) {
        // Use the component constructor to inject providers.
    }


    ngOnInit(): void {
        this.store.select((state) => state.noticias.sugerida)
            .subscribe((data) => {
                const f = data;
                if (f != null) {
                    Toast.show({text: "Sugerimos leer: " + f.titulo, duration: Toast.DURATION.SHORT});
                }
            });
        /*this.noticias.agregar("hola!");
        this.noticias.agregar("hola2!");
        this.noticias.agregar("hola3!");*/
    }

    onDrawerButtonTap(): void {
        const sideDrawer = <RadSideDrawer>app.getRootView();
        sideDrawer.showDrawer();
    }

    onItemTap(args): void{
        //console.log(x);
        this.store.dispatch(new NuevaNoticiaAction(new Noticia(args.view.bindingContext)));
    }

    onLongPress(s): void {
        console.log(s);
        SocialShare.shareText(s, "Asunto: compartido desde el curso!");
    }

    buscarAhora(s: string) {
        /*this.resultados = this.noticias.buscar()
                                .filter(x => x.indexOf(s) >= 0);

        const layout = <View>this.layout.nativeElement;
        layout.animate({
            backgroundColor: new Color("blue"),
            duration: 3000,
            delay: 1500
        }).then(() => layout.animate({
            backgroundColor: new Color("white"),
            duration: 3000,
            delay: 1500
        }));*/

        console.dir("buscarAhora" + s);
        this.noticias.buscar(s).then((r: any) => {
            console.log("resultados buscarAhora: " + JSON.stringify(r));
            this.resultados = r;
        }, (e) => {
            console.log("error buscarAhora " + e);
            Toast.show({text: "Error en la búsqueda", duration: Toast.DURATION.SHORT});
        });
    }
}
